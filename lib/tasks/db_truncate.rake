namespace :db do

  def filtered_tables
    excluded_tables =  ['schema_migrations', 'users']
    ActiveRecord::Base.connection.tables.reject do |table|
      excluded_tables.include?(table)
    end
  end

  desc "Truncate all existing data"
  task :truncate => ["db:load_config", :environment] do

    begin
      config = ActiveRecord::Base.configurations[::Rails.env]
      ActiveRecord::Base.establish_connection
      filtered_tables.each do |table|
        ActiveRecord::Base.connection.execute("TRUNCATE #{table}")
      end
    end

  end

end


