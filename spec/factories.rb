FactoryGirl.define do
   factory :product do
     title "Product"
     description "Product description"
     price 10.00

     factory :brand do
       name "brand"
     end

     factory :category do
       name "category"
     end
   end
 end
