require 'spec_helper'

describe 'homepage' do
  it 'displays homepage' do
    get "/"
    assert_response :success
  end

  it 'welcomes the user' do
    visit '/'
    page.should have_content('Welcome')
  end

  it 'displays featured products' do
    product = FactoryGirl.create(:product)
    visit '/'
    page.should have_selector('div#featured_products')
    page.should have_css('div#featured_products .productThumbWrapper', :count => 1)
    #page.should have_xpath('//div[@id="featured_products"]/div[contains(@class, "productThumbWrapper")]')
    #page.should find_by_id(:featured_products).have_xpath('/')
  end

end
