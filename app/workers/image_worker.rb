class ImageWorker
  include Sidekiq::Worker

  def perform(id, remote_image_net_url)
    product_image = ProductImage.find(id)
    product_image.remote_image_url = remote_image_net_url
    product_image.save!
  end
end
