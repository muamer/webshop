class Admin::ProductImagesController < ApplicationController

  before_filter :authorize!

  def create
    @admin_product = Product.find(params[:product_id])
    @product_image = @admin_product.product_images.build(params[:product_image])

    respond_to do |format|
      if @product_image.save
        format.js
      else
        format.js
      end
    end
  end

  def destroy
    @admin_product = Product.find(params[:product_id])
    @product_image = ProductImage.find(params[:id])

    respond_to do |format|
      if @product_image.destroy
        format.html {redirect_to edit_admin_product_path(@admin_product), notice: "Product Image Deleted!"}
      else
        format.html {redirect_to edit_admin_product_path(@admin_product), notice: "Cannot delete image! Contact Administrator."}
      end
    end
  end
end
