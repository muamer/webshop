class Admin::BrandsController < ApplicationController

  layout 'admin'
  before_filter :authorize!

  # GET /admin/brands
  # GET /admin/brands.json
  def index
    @admin_brands = Brand.all_brands

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_brands }
    end
  end


  # GET /admin/brands/new
  # GET /admin/brands/new.json
  def new
    @admin_brand = Brand.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @admin_brand }
    end
  end

  # GET /admin/brands/1/edit
  def edit
    @admin_brand = Brand.find(params[:id])
  end

  # POST /admin/brands
  # POST /admin/brands.json
  def create
    @admin_brand = Brand.new(params[:brand])

    respond_to do |format|
      if @admin_brand.save
        format.html { redirect_to admin_brands_url, notice: 'Brand was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /admin/brands/1
  # PUT /admin/brands/1.json
  def update
    @admin_brand = Brand.find(params[:id])

    respond_to do |format|
      if @admin_brand.update_attributes(params[:brand])
        format.html { redirect_to admin_brands_url, notice: 'Brand was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @admin_brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/brands/1
  # DELETE /admin/brands/1.json
  def destroy
    @admin_brand = Brand.find(params[:id])
    @admin_brand.destroy

    respond_to do |format|
      format.html { redirect_to admin_brands_url }
      format.json { head :no_content }
    end
  end
end
