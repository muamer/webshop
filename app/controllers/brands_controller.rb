class BrandsController < ApplicationController
  def show
    @brand = Brand.find(params[:id])

    respond_to do |format|
      format.html
    end
  end
end
