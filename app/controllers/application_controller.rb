class ApplicationController < ActionController::Base
  protect_from_forgery

private
  
  def current_user
    @current_user = User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize!
    redirect_to root_url, alert: "You are not authorized to access this area!" if current_user.nil?
  end

end
