# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('#fileupload').fileupload
    add: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png)$/i
      file = data.files[0]
      console.log "Added file", file
      if types.test(file.type) || types.test(file.name)
        data.form.find('#Content-Type').val(file.type)
        data.context = $(upload_template(file))
        $('#upload_templates_wrapper').append(data.context)
        data.submit()
      else
        alert("#{file.name} is not a gif, jpeg, or png image file")
    
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.meter').css('width', progress + '%')
    
    done: (e, data) ->
      file = data.files[0]
      domain = $('#fileupload').attr('action')
      path = $('#fileupload input[name=key]').val().replace('${filename}', file.name)
      to = $('#fileupload').data('post')
      content = {}
      content[$('#fileupload').data('as')] = domain + path

      content['key'] = path
      $.post(to, content)
      data.context.remove() if data.context # remove progress bar
    
    fail: (e, data) ->
      alert("#{data.files[0].name} failed to upload.")
      console.log("Upload failed:")
      console.log(data)

#@add_upload_template = (file, context) ->
  #reader = new FileReader()
  #_context = context
  #console.log "_context", context
  #reader.onloadend = (e) ->
    #image = $('<img>').attr 'src', e.target.result
    #context = "asdasd"
  #console.log file
  #reader.readAsDataURL(file)

@upload_template = (file) ->
  upload_template =
    """
    <div class='uploadTemplateWrapper'>
      <label>Uploading file:</label>
      <p>#{file.name}</p>
      <div class="progress six">
        <div class="meter"></div>
      </div>
    </div>
    """
