errors = <%= raw @product_image.errors_to_json %>

if errors.length == 0
  uploaded_image_thumb = """
  <%= j render '/admin/product_images/product_thumb_raw', image: @product_image %>
  """
  console.log uploaded_image_thumb
  $("#product_images").append(uploaded_image_thumb)
else
  alert errors
