class ProductImage < ActiveRecord::Base
  belongs_to :product
  attr_accessible :image, :remote_image_url

  mount_uploader :image, ImageUploader

  def errors_to_json
    errors.full_messages.to_json
  end

  #after_save :enqueue_image

  #def enqueue_image
    #ImageWorker.perform_async(id, remote_image_net_url) if remote_image_net_url.present?
  #end
end
