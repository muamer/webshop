class User < ActiveRecord::Base
  has_secure_password
  # In order to crate new user provide email, password and password_confirmation fields

  attr_accessible :email, :password, :password_confirmation
  validates :email, :uniqueness => true
end
