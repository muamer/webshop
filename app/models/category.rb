class Category < ActiveRecord::Base
  attr_accessible :name

  before_save :create_slug

  has_many :products

  def create_slug
    self.slug = self.name.parameterize
  end

  def self.all_categories
    Category.order('name')
  end
end
