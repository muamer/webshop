class Product < ActiveRecord::Base
  attr_accessible :description, 
                  :title, 
                  :category_id, 
                  :brand_id,
                  :price

  validates :title, :presence => true
  validates :description, :presence => true
  validates :price, :presence => true,
            :numericality => true,
            :format => { :with => /^\d{1,4}(\.\d{0,2})?$/ }

  has_many :product_images
  belongs_to :category
  belongs_to :brand

  # get featuring image
  def featuring_image(size)
    product_images.first.image_url(size) if product_images.first
  end
end
