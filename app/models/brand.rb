class Brand < ActiveRecord::Base
  attr_accessible :slug, :name

  before_save :create_slug

  has_many :products

  def create_slug
    self.slug = self.name.parameterize
  end

  def self.all_brands
    Brand.order('name')
  end
end
