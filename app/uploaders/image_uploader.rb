# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base
  #include CarrierWaveDirect::Uploader
  include CarrierWave::RMagick

  include CarrierWave::MimeTypes
  process :set_content_type

  storage :fog

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process :resize_to_fill => [220, 220]
  end

  version :large do
    process :resize_to_fit => [440, nil]
  end

  version :scaled do
    process :resize_to_fit => [840, nil]
  end

end
